﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hacker : MonoBehaviour
{
	// Game state
	enum Screen { MainMenu, Password, Win }
	private Screen _currentScreen = Screen.MainMenu;

	enum GameLevel { Easy, Medium, Hard, Unchosen }
	private GameLevel _currentGameLevel = GameLevel.Unchosen;

	[SerializeField] private string[] _EasyPasswords;
	[SerializeField] private string[] _MediumPasswords;
	[SerializeField] private string[] _HardPasswords;
	private string _chosenPassword;


    // Use this for initialization
    void Start()
    {
        ShowMainMenu();
    }

    private void OnUserInput(string input)
    {

        if (input == "menu") // We can always go direct to the main menu
        {
            ShowMainMenu();
			_currentScreen = Screen.MainMenu;
			_currentGameLevel = GameLevel.Unchosen;
        }
		else if (_currentScreen == Screen.MainMenu)
		{
			RunMainMenu(input);
		}
		else if (_currentScreen == Screen.Password)
		{
			PlayGame(input, _chosenPassword);
		}
		else if (_currentScreen == Screen.Win)
		{
			EndGame(input);
		}
    }

	private void RunMainMenu(string input)
	{
		bool isValidLevel = (input == "1" || input == "2" || input == "3");
		if (isValidLevel)
		{
			StartGame(int.Parse(input));
		}
		else
		{
			Terminal.WriteLine("Please choose right level");
		}
	}

	private string SelectPasswordsForLevel(GameLevel level)
	{
		if (level == GameLevel.Easy)
		{
			return _EasyPasswords[Random.Range(0, _EasyPasswords.Length)];
		}
		else if (level == GameLevel.Medium)
		{
			return _MediumPasswords[Random.Range(0, _MediumPasswords.Length)];
		}
		else if (level == GameLevel.Hard)
		{
			return _HardPasswords[Random.Range(0, _HardPasswords.Length)];
		}	
		return null;
	}

	private void PlayGame(string inputPassword, string _chosenPassword)
	{

        if (inputPassword == _chosenPassword)
        {
			DisplayWinScreen();	
        }
        else
        {
            Terminal.WriteLine("Sorry, wrong password!");
        }
	}

	private void DisplayWinScreen()
    {
        _currentScreen = Screen.Win;
		Terminal.ClearScreen();
		ShowLevelReward();
        Terminal.WriteLine("Do you want to play again? (y/n):");
    }

	private void ShowLevelReward()
	{
		switch (_currentGameLevel)
		{
			case GameLevel.Easy:
				Terminal.WriteLine("Congrats, you win on easy level, try another one in this level of jupm to next level!");
				break;
			case GameLevel.Medium:
				Terminal.WriteLine("This is the win for u at the Medium level, Hard is ahead, if u want more challnge!");
				break;
			case GameLevel.Hard:
				Terminal.WriteLine("FINAL BOSS DEFEATED!!");
				break;
		}
	}

	private void StartGame(int gameLevel)
	{
		_currentScreen = Screen.Password;
		Terminal.ClearScreen();
		switch (gameLevel)
		{
			case 1:
				_currentGameLevel = GameLevel.Easy;
				_chosenPassword = SelectPasswordsForLevel(_currentGameLevel);
				break;
			case 2:
				_currentGameLevel = GameLevel.Medium;
				_chosenPassword = SelectPasswordsForLevel(_currentGameLevel);
				break;
			case 3:
				_currentGameLevel = GameLevel.Hard;
				_chosenPassword = SelectPasswordsForLevel(_currentGameLevel);
				break;
		}
		Terminal.WriteLine("Enter your password, hint: " + _chosenPassword.Anagram());
	}

	private void EndGame(string answer)
	{
    	if (answer == "y" )
        {
			ShowMainMenu();
			_currentScreen = Screen.MainMenu;
			_currentGameLevel = GameLevel.Unchosen;
        }
		else if (answer == "n")
		{
			Application.Quit();
		}
		else
		{
			Terminal.WriteLine("Enter correct answer (y/n)");
		}

    }

    private void ShowMainMenu()
    {
        Terminal.ClearScreen();
        Terminal.WriteLine("Hacker Treminal. 2018 (c)");
        Terminal.WriteLine("");
        Terminal.WriteLine("Select your hacking skills level:");
        Terminal.WriteLine("");
        Terminal.WriteLine("Press 1 to select 'Newbie Hacker'");
        Terminal.WriteLine("Press 2 to select 'Expirienced Hacker'");
        Terminal.WriteLine("Press 3 to select 'Hacker Magician'");
        Terminal.WriteLine("");
        Terminal.WriteLine("Enter selection:");
    }
}
